


# ATM-api-client-python
Python-skriptejä, jotka keskustelevat Jiran ATM:n (Adaptavist Test Management) REST API:n kanssa. Skriptejä käytetään siirtämään automaatiotestitulokset ATM:ään. 

### Asennettavat pythonkirjastot/moduulit
* xml.etree.ElementTree
* argparse
* requests

## Usetrace
**Usetrace.py** hakee viimeisimpiä automaatiotestien tuloksia Usetracen rajapinnasta XML-muodossa, parsii ja jäsentää tulokset, ja lähetäää ne Jiran ATM:ään. Skripti käyttää viimeisimpien tuloksien hakemiseen Usetracen batchid:tä (GET https://api.usetrace.com/api/results/<BATCH_ID>/xunit), joka saadaan testien käynnistyksen tuloksena. 

Skripti luo dictionaryn, johon lisätään ajettujen testien nimet PASS/FAIL- statuksella. Tämän jälkeen tarkistetaan Jiran rajapinnasta GET-kyselyllä löytyykö jo samannimisiä tetsicaseja, jos ei löydy, niin luodaan uusi testicase Jiraan. Tämän jälkeen luodaan testiruni, joka nimetään testatun tuotteen julkaisunumerolla ja liitetään siihen teticaset. Luotu testiruni löytyy ATM:n polusta: /usetraceResults.

**config.py** sisältää ATM:n autentikointiin liittyvää tietoa (käyttäjätunnus, salasana, Jiran base-URL).

### Muutettavaa
* Tiedosto congif.py sisältää ATM:n autentikointiin liittyyvää tietoa ja sinne on lisättävä muuttujat **USERNAME** ja **PASSWORD**. 
* Usetrace.py tiedosto sisältää muuttujan **project**, joka muutetaan vastaamaan käytetyn ATM-projektin ID:tä. 


### Käynnistys
Skripti ottaa käynnistyksen yhteydessä 2 argumenttia: BATCH_ID (palauttaa usetracetulokset), RELEASE (nimeää luodun Runin tämän mukaan).

```
python usetrace.py <BATCH_ID> <RELEASE>
```

### Dokumentointi
* [ATM REST API](https://www.kanoah.com/docs/public-api/1.0/) 
* [Usetace API](http://docs.usetrace.com/api/) 

## Robot Framework

**RobotFramework.py** parsii Robot Frameworkin tuottamaa XML-tiedostoa ja siirtää dataa Jiran ATM:ään. Toimii samalla logiikalla kuin yllä avattu usetrace.py-skripti, paitsi skripti käyttää datalähteenä lokaalisti tallennettua RF:N XML-tiedostoa. 

**config.py** sisältää ATM:n autentikointiin liittyvää tietoa (käyttäjätunnus, salasana, Jiran base-URL).

### Muutettavaa
* Tiedosto congif.py sisältää ATM:n autentikointiin liittyyvää tietoa ja sinne on lisättävä muuttujat **USERNAME** ja **PASSWORD**. 

### Käynnistys
Skripti ottaa käynnistyksen yhteydessä 3 argumenttia: file (Robot Frameworkin tulostiedoston nimi), project (Jiran ATM:n projektin ID), testrun (haluttu testirunin nimi)

```
python RobotFramework.py <file> <project> <testrun>
```
