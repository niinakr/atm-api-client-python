"""Creates Kanoah Tests test runs from Robot Framework output."""
import xml.etree.ElementTree as ET
import argparse
import config
import requests
from requests.auth import HTTPBasicAuth


KANOAH_SEARCH_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testcase/search'
KANOAH_TESTCASE_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testcase'
KANOAH_TESTRUN_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testrun'


def get_json_or_raise(response):
    response.raise_for_status()
    return response.json()


def results_to_kanoah_tests(input_file, project_key, auth):
    """Parse test case results from the input file and return a list of the results."""
    test_results = []
    tree = ET.parse(input_file)
    root = tree.getroot()

    def create_test_results(parent_node, parent_name):
        """Parse results for each test case in the test suites.
        Create the test case on Kanoah if it does not exist.
        """
        for suite in parent_node.findall('suite'):
            suite_name = suite.get('name')
            if parent_name:
                suite_name = '.'.join((parent_name, suite_name))

            for test in suite.findall('test'):
                name = '.'.join((suite_name, test.get('name')))
                print('Processing: ' + name)
                query = 'projectKey = "{}" AND name = "{}"'.format(project_key, name)
                params = {'query': query, 'fields': 'key,name'}
                response = requests.get(KANOAH_SEARCH_URL, params=params, auth=auth, verify=False)
                content = get_json_or_raise(response)

                # Get the test case by the exact name (Kanoah returns partial matches)
                test_case = next((item for item in content if item['name'] == name), None)
                if not test_case:
                    # Create missing test case on Kanoah
                    payload = {'projectKey': project_key, 'name': name}
                    response = requests.post(KANOAH_TESTCASE_URL, json=payload, auth=auth, verify=False)
                    test_case = get_json_or_raise(response)

                key = test_case['key']
                status = test.find('status').get('status').capitalize()
                result = {'testCaseKey': key, 'status': status}
                test_results.append(result)
            create_test_results(suite, suite_name)

    create_test_results(root, '')
    return test_results


def create_test_run(project_key, name, test_cases, auth):
    """Create a test run with the given test cases on Kanoah."""
    payload = {'projectKey': project_key, 'name': name, 'items': test_cases}
    response = requests.post(KANOAH_TESTRUN_URL, json=payload, auth=auth, verify=False)
    run_id = get_json_or_raise(response)['key']
    count = len(test_cases)
    print('Test run {} created ({} {})'.format(run_id, count, 'test' if count == 1 else 'tests'))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str, help='Name of the Robot Framework output XML file')
    parser.add_argument('project', type=str, help='JIRA project key')
    parser.add_argument('testrun', type=str, help='Name of the test run')
    args = parser.parse_args()

    auth = HTTPBasicAuth(config.USERNAME, config.PASSWORD)
    test_cases = results_to_kanoah_tests(args.file, args.project, auth)
    create_test_run(args.project, args.testrun, test_cases, auth)


if __name__ == "__main__":
    main()