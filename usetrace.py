#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Creates Kanoah Tests test runs from usetrace output."""
import xml.etree.ElementTree as ET
import argparse
import config
import requests
from requests.auth import HTTPBasicAuth

KANOAH_SEARCH_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testcase/search'
KANOAH_TESTCASE_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testcase'
KANOAH_TESTRUN_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testrun'

project = "changeme"
folder = "/usetraceResults"


def get_json_or_raise(response):
    response.raise_for_status()
    return response.json()

def parseTest_result(batchid):
    response = requests.get('http://api.usetrace.com/api/results/'+ batchid +'/xunit')
    tree = ET.ElementTree(ET.fromstring(response.content))
    root = tree.getroot()

    testCaseNames = []

   
    for testcase in root.findall('testcase'):
        testcase_name = testcase.get('name')
        error = testcase.find('error')
        if error is None:
           result ={'Name': testcase_name, 'Status': 'Pass'}
           testCaseNames.append(result)
               
        else:
            result ={'Name': testcase_name, 'Status': 'Fail'}
            testCaseNames.append(result)
                

    return testCaseNames
    

def create_test_cases(project_key, test_cases, auth):
    test_results = []
    for i, entry in enumerate(test_cases):
                name = entry['Name']
                status = entry['Status']
                query = 'projectKey = "{}" AND name = "{}"'.format(project_key, name)
                params = {'query': query, 'fields': 'key,name'}
                response = requests.get(KANOAH_SEARCH_URL, params=params, auth=auth, verify=False)
                content = get_json_or_raise(response)

                # Get the test case by the exact name (Kanoah returns partial matches)
                test_case = next((item for item in content if item['name'] == name), None)
                if not test_case:
                    # Create missing test case on Kanoah
                    payload = {'projectKey': project_key, 'name': name}
                    response = requests.post(KANOAH_TESTCASE_URL, json=payload, auth=auth, verify=False)
                    test_case = get_json_or_raise(response)
                    
                key = test_case['key']
                result = {'testCaseKey': key, 'status': status}
                test_results.append(result)
                
    return test_results


def create_test_run(project_key, name, test_cases, auth):
    """Create a test run with the given test cases on Kanoah."""
    payload = {'projectKey': project_key, 'name': name, 'folder': folder, 'items': test_cases}
    response = requests.post(KANOAH_TESTRUN_URL, json=payload, auth=auth, verify=False)
    run_id = get_json_or_raise(response)['key']
    count = len(test_cases)
    print('Test run {} created ({} {})'.format(run_id, count, 'test' if count == 1 else 'tests'))
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('batchid', type=str, help='ID of the Usetrace result')
    parser.add_argument('release', type=str, help='Release of the deployment')
    args = parser.parse_args()
    
    auth = HTTPBasicAuth(config.USERNAME, config.PASSWORD)
    test_cases = parseTest_result(args.batchid)
    test_results = create_test_cases(project, test_cases, auth)
    testrun= 'UsetraceResult-'+ args.release + '-Release'
    create_test_run(project, testrun, test_results, auth)

    
if __name__ == "__main__":
    main()
